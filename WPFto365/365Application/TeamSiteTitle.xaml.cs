﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.SharePoint.Client;
using System.Security;
using OfficeDevPnP.Core.Sites;
using Microsoft.Online.SharePoint.TenantAdministration;
using Microsoft.SharePoint;
using Oasys.Wpf.Themes.CustomWindow;
using System.Windows.Media.Animation;

namespace _365Application
{
    /// <summary>
    /// Interaction logic for TeamSiteTitle.xaml
    /// </summary>
    public partial class TeamSiteTitle : OasysWindow
    {
        public TeamSiteTitle()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string usname = Login.user;
            string password = Login.pass;

            string siteTitle = textsite.Text;

         
            try
            {
               
                CreateteamSite(usname, password, siteTitle);
             
                
            }
            catch (Exception ex)
            {
                MessageBox.Show($" There was an exception: {ex.ToString()}");

            }
        }

        public  void CreateteamSite(string usname, string Password, string siteTitle)
        {
           
            string UserName = usname;
            using (ClientContext context = new ClientContext("https://lahorecity-admin.sharepoint.com"))
            {
                SecureString passWord = new SecureString();
                foreach (char c in Password.ToCharArray()) passWord.AppendChar(c);
                context.Credentials = new SharePointOnlineCredentials(UserName, passWord);
                
                try
                {
                    // If the site is subsite of a subsite, then use OpenWeb as show below in comment
                    // ctx.Site.OpenWeb("top-subsite-url-name").WebExists(subSite);
                    // Check If Subsite existing
                    if (context.Site.RootWeb.WebExists(siteTitle))
                    {
                        MessageBox.Show("Already Exists");
                    }
                    else
                    {
                        var tenant = new Tenant(context);

                        //Create new site.
                        var newsite = new SiteCreationProperties()
                        {
                            Url = "https://lahorecity.sharepoint.com/sites/" + siteTitle,
                            Owner = usname,
                            Template = "STS#0",
                            Title = siteTitle,
                            StorageMaximumLevel = 1000,
                            StorageWarningLevel = 500,
                            TimeZoneId = 7,
                        };

                        var spoOperation = tenant.CreateSite(newsite);

                        context.Load(spoOperation);
                        context.ExecuteQuery();


                        if (!spoOperation.IsComplete)
                        {
                            
                            context.Load(spoOperation);
                            context.ExecuteQuery();
                          
                            System.Threading.Thread.Sleep(20000);
                             MessageBox.Show("" + (spoOperation.IsComplete ? "waiting" : "Site has been Created Successfully")); 
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Something went wrong. " + ex.Message);
                }
            }
            
        }
      
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MainWindow obj = new MainWindow();
            obj.Show();
            this.Hide();
        }
    }
    }

