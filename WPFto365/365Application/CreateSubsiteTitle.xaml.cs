﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint;
using System.Security;
using OfficeDevPnP.Core.Sites;

namespace _365Application
{
    using Microsoft.Online.SharePoint.TenantAdministration;
    using Oasys.Wpf.Themes.CustomWindow;
    /// <summary>
    /// Interaction logic for CreateSubsiteTitle.xaml
    /// </summary>
    public partial class CreateSubsiteTitle : OasysWindow
    {
        public CreateSubsiteTitle()
        {
            InitializeComponent();
            allsite(Login.user, Login.pass);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string usname = Login.user;
            string password = Login.pass;

            string siteTitle = textsite1.Text;
            string subsitename = combo2.Text;
            string descriptin = textdescription.Text;

            try
            {
                CreateSubSite(usname, password, siteTitle, subsitename, descriptin);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show($" There was an exception: {ex.ToString()}");

            }

        }
        public  void CreateSubSite(string usname, string Password, string siteTitle, string subsitename, string description)
        {
            using (ClientContext context = new ClientContext("https://lahorecity.sharepoint.com/sites/" + subsitename))
            {
                SecureString passWord = new SecureString();
                foreach (char c in Password.ToCharArray()) passWord.AppendChar(c);
                context.Credentials = new SharePointOnlineCredentials(usname, passWord);
                try
                {
                    // If the site is subsite of a subsite, then use OpenWeb as show below in comment
                    // ctx.Site.OpenWeb("top-subsite-url-name").WebExists(subSite);
                    // Check If Subsite existing
                    if (context.Site.RootWeb.WebExists(siteTitle))
                    {
                        MessageBox.Show("Already Exists");
                    }
                    else
                    {


                        WebCreationInformation creation = new WebCreationInformation();
                        creation.Title = siteTitle;
                        creation.Url = siteTitle;
                        creation.Description = description;
                        creation.WebTemplate = "STS#0";
                        Web newWeb = context.Web.Webs.Add(creation);
                        context.ExecuteQuery();
                        MessageBox.Show("Subsite Created");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Something went wrong. " + ex.Message);
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
            MainWindow obj = new MainWindow();
            obj.Show();
            
        }
        public void allsite(string usename, string Pasesword)
        {
            using (ClientContext context = new ClientContext("https://lahorecity-admin.sharepoint.com"))
            {
                SecureString passWord = new SecureString();
                foreach (char c in Pasesword.ToCharArray()) passWord.AppendChar(c);
                context.Credentials = new SharePointOnlineCredentials(usename, passWord);
                Tenant tenant = new Tenant(context);
                SPOSitePropertiesEnumerable siteProps = tenant.GetSitePropertiesFromSharePoint("0", true);
                context.Load(siteProps);
                context.ExecuteQuery();
            //    MessageBox.Show("Total Site Collections: " + siteProps.Count.ToString());
                foreach (var site in siteProps)
                {
                    //Console.WriteLine(site.Title + "\t" + site.Template.ToString());
                    //    MessageBox.Show(site.Title + ":" + site.Template.ToString());
                    //   listbox.Items.Add(TableRow[site.Title + ":" + site.Template.ToString()]);
                    // control_name.Items.Add(item.ToString());
                    combo2.Items.Add(site.Title.ToString());



                }

            }
        }
        public void Combo2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void Textsite1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
