﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _365Application
{
    using Oasys.Wpf.Themes.CustomWindow;
    /// <summary>
    /// Interaction logic for SiteCreater.xaml
    /// </summary>
    public partial class SiteCreater : OasysWindow
    {
        public SiteCreater()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            TeamSiteTitle obj = new TeamSiteTitle();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            obj.Show();
            this.Hide();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            CreateSubsiteTitle obj = new CreateSubsiteTitle();
            obj.Show();
            this.Hide();
        }
    }
}
