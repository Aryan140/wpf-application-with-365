﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.SharePoint.Client;
using System.Security;
using Microsoft.SharePoint;
using OfficeDevPnP.Core.Sites;
using Microsoft.Online.SharePoint.TenantAdministration;
namespace _365Application
{
    using Oasys.Wpf.Themes.CustomWindow;
    /// <summary>
    /// Interaction logic for UserPermissions.xaml
    /// </summary>
    public partial class UserPermissions : OasysWindow
    {
        public UserPermissions()
        {
            InitializeComponent();
            allsite(Login.user, Login.pass);
        }
        public string grpnamevalue = "";

        public class Employee
        {

            public string EmpName { get; set; }
        }


        public void allsite(string usename, string Pasesword)
        {
            using (ClientContext context = new ClientContext("https://lahorecity-admin.sharepoint.com"))
            {
                SecureString passWord = new SecureString();
                foreach (char c in Pasesword.ToCharArray()) passWord.AppendChar(c);
                context.Credentials = new SharePointOnlineCredentials(usename, passWord);
                Tenant tenant = new Tenant(context);
                SPOSitePropertiesEnumerable siteProps = tenant.GetSitePropertiesFromSharePoint("0", true);
                context.Load(siteProps);
                context.ExecuteQuery();
                //  MessageBox.Show("Total Site Collections: " + siteProps.Count.ToString());
                foreach (var site in siteProps)
                {
                    //Console.WriteLine(site.Title + "\t" + site.Template.ToString());
                    //    MessageBox.Show(site.Title + ":" + site.Template.ToString());
                    //   listbox.Items.Add(TableRow[site.Title + ":" + site.Template.ToString()]);
                    // control_name.Items.Add(item.ToString());
                    comboSC.Items.Add(site.Title.ToString());



                }

            }
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
          //  allsite("Aryan@lahorecity.onmicrosoft.com", "Lahore140");



        }

        private void ComboSC_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            string usname = Login.user;
            string password = Login.pass;
            dgEmp.Items.Clear();

            Getgroups(usname, password, comboSC.SelectedItem.ToString());

            UserGrid.Items.Clear();

            label1.Content = "";
            label2.Content = "";
        }

        public void Getgroups(string usename, string Pasesword, string sitename)
        {
            using (ClientContext context = new ClientContext("https://lahorecity.sharepoint.com/sites/" + sitename))
            {
                SecureString passWord = new SecureString();
                foreach (char c in Pasesword.ToCharArray()) passWord.AppendChar(c);
                context.Credentials = new SharePointOnlineCredentials(usename, passWord);

                Web web = context.Web;
                //Parameters to receive response from the server    
                //SiteGroups property should be passed in Load method to get the collection of groups    
                context.Load(web, w => w.Title, w => w.SiteGroups);
                context.ExecuteQuery();

                GroupCollection groups = web.SiteGroups;

                //       MessageBox.Show("Groups associated to the site: " + web.Title);
                //       MessageBox.Show("Groups Count: " + groups.Count.ToString());
                foreach (Group grp in groups)
                {
                    //   MessageBox.Show(grp.Title);
                    // combogroups.Items.Add(grp.Title);
                    //Add(new Employee() { EmpNo = 101, EmpName = "Aryan", Designation = "Manager", Salary = 500000 });
                    //   grp.Title = grouphandle;
                    // dgEmp.Items.Add(grp.Title + " members");
                    dgEmp.Items.Add(new Employee() { EmpName = grp.Title });
                }


            }
        }

        public class users
        {

            public string username { get; set; }
        }

        public void Userss_Click(object sender, RoutedEventArgs e)
        {
            string usname = Login.user;
            string password = Login.pass;
            try
            {
                UserGrid.Items.Clear();
                Employee emp = (Employee)dgEmp.CurrentItem;
                string sitn = comboSC.Text;
                grpnamevalue = emp.EmpName;
                label1.Content = emp.EmpName + " Users";
                label2.Content = "Add Users to " + emp.EmpName;
                allusers(usname, password, sitn, emp.EmpName);

            }
            catch (Exception ex)
            {
                MessageBox.Show($" There was an exception: {ex.ToString()}");

            }

        }
        public void allusers(string usename, string Pasesword, string sitename, string groupname)
        {
            using (ClientContext context = new ClientContext("https://lahorecity.sharepoint.com/sites/" + sitename))
            {
                SecureString passWord = new SecureString();
                foreach (char c in Pasesword.ToCharArray()) passWord.AppendChar(c);
                context.Credentials = new SharePointOnlineCredentials(usename, passWord);

                GroupCollection siteGroups = context.Web.SiteGroups;

                // Assume that there is a "Members" group, and the ID=5. 
                // Group membersGroup = siteGroups.GetById(8);
                Group membersGroup = context.Web.SiteGroups.GetByName(groupname);
                context.Load(membersGroup.Users);
                context.ExecuteQuery();

                foreach (User member in membersGroup.Users)
                {
                    // We have all the user info. For example, Title. 
                    //label1.Text = label1.Text + ", " + member.Title;
                    // MessageBox.Show(member.Title);
                    // combouser.Items.Add(member.Title);
                    UserGrid.Items.Add(new users() { username = member.Title });
                }

            }
        }

        public void DgEmp_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


        }

        private void Btnuseradd_Click(object sender, RoutedEventArgs e)
        {
            try
            {



                //     adduser("Aryan@lahorecity.onmicrosoft.com", "Lahore140", comboSC.Text, emp.EmpName);

            }
            catch (Exception ex)
            {
                MessageBox.Show($" There was an exception: {ex.ToString()}");

            }
        }
        public void adduser(string usename, string Pasesword, string sitenam, string groupname)
        {
            using (ClientContext context = new ClientContext("https://lahorecity.sharepoint.com/sites/" + sitenam))
            {
                SecureString passWord = new SecureString();
                foreach (char c in Pasesword.ToCharArray()) passWord.AppendChar(c);
                context.Credentials = new SharePointOnlineCredentials(usename, passWord);

                /*   // Get group object using group name
                   Group oGroup = context.Web.SiteGroups.GetByName("Demo Members");

                   // Get user using Logon name
                   User oUser = context.Web.EnsureUser("Aryan Khan");

                   context.Load(oUser);
                  context.ExecuteQuery();

                   // Remove user from the group
                   oGroup.Users.RemoveByLoginName(oUser.LoginName);

                   context.ExecuteQuery(); */
                // Get group object using group name
                Group oGroup = context.Web.SiteGroups.GetByName(groupname);

                // Get user using Logon name
                User oUser = context.Web.EnsureUser(textuseradd.Text);

                // Add user to the group
                oGroup.Users.AddUser(oUser);

                context.ExecuteQuery();

            }
        }

        private void DelUser_Click(object sender, RoutedEventArgs e)
        {
            string usname = Login.user;
            string password = Login.pass;
            try
            {

                Employee emp = (Employee)dgEmp.CurrentItem;
                users obj = (users)UserGrid.CurrentItem;
                string sitn = comboSC.Text;

                deleteuser(usname, password, grpnamevalue, obj.username);


            }
            catch (Exception ex)
            {
                MessageBox.Show($" There was an exception: {ex.ToString()}");

            }
        }
        public void deleteuser(string usename, string Pasesword, string gpname, string logonname)
        {
            using (ClientContext context = new ClientContext("https://lahorecity.sharepoint.com/sites/" + comboSC.Text))
            {
                SecureString passWord = new SecureString();
                foreach (char c in Pasesword.ToCharArray()) passWord.AppendChar(c);
                context.Credentials = new SharePointOnlineCredentials(usename, passWord);

                // Get group object using group name
                Group oGroup = context.Web.SiteGroups.GetByName(gpname);

                // Get user using Logon name
                User oUser = context.Web.EnsureUser(logonname);

                context.Load(oUser);
                context.ExecuteQuery();

                // Remove user from the group
                oGroup.Users.RemoveByLoginName(oUser.LoginName);

                context.ExecuteQuery();

                // Get group object using group name
                /* Group oGroup = context.Web.SiteGroups.GetByName(combogroups.Text);

                 // Get user using Logon name
                 User oUser = context.Web.EnsureUser(combouser.Text);

                 // Add user to the group
                 oGroup.Users.AddUser(oUser);

                 context.ExecuteQuery(); */

            }
        }

        private void Btnuseradd_Click_1(object sender, RoutedEventArgs e)
        {
            string usname = Login.user;
            string password = Login.pass;
            try
            {



                adduser(usname, password, comboSC.Text, grpnamevalue);

            }
            catch (Exception ex)
            {
                MessageBox.Show($" There was an exception: {ex.ToString()}");

            }
        }
    }
}
