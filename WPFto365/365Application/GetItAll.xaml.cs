﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _365Application
{
    using Microsoft.Online.SharePoint.TenantAdministration;
    using Microsoft.SharePoint.Client;
    using Oasys.Wpf.Themes.CustomWindow;
    using System.Security;

    /// <summary>
    /// Interaction logic for Window2.xaml
    /// </summary>
    public partial class GetItAll : OasysWindow
    {
        public GetItAll()
        {
            InitializeComponent();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            string usename = Login.user;
            string pasesword = Login.pass;




            try
            {
                allsite(usename, pasesword);

            }
            catch (Exception ex)
            {
                MessageBox.Show($" There was an exception: {ex.ToString()}");

            }
        }
        public void allsite(string usename, string Pasesword)
        {
            using (ClientContext context = new ClientContext("https://lahorecity-admin.sharepoint.com"))
            {
                SecureString passWord = new SecureString();
                foreach (char c in Pasesword.ToCharArray()) passWord.AppendChar(c);
                context.Credentials = new SharePointOnlineCredentials(usename, passWord);
                Tenant tenant = new Tenant(context);
                SPOSitePropertiesEnumerable siteProps = tenant.GetSitePropertiesFromSharePoint("0", true);
                context.Load(siteProps);
                context.ExecuteQuery();
                //  MessageBox.Show("Total Site Collections: " + siteProps.Count.ToString());
                foreach (var site in siteProps)
                {
                    //Console.WriteLine(site.Title + "\t" + site.Template.ToString());
                    //    MessageBox.Show(site.Title + ":" + site.Template.ToString());
                    //   listbox.Items.Add(TableRow[site.Title + ":" + site.Template.ToString()]);
                    // control_name.Items.Add(item.ToString());
                    comboSC.Items.Add(site.Title.ToString());



                }

            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string usename = Login.user;
            string pasesword = Login.pass;

            string sitename = comboSC.Text;


            try
            {
                allusers(usename, pasesword , sitename);

            }
            catch (Exception ex)
            {
                MessageBox.Show($" There was an exception: {ex.ToString()}");

            }

        }
        public void allusers(string usename, string Pasesword, string sitename)
        {
            using (ClientContext context = new ClientContext("https://lahorecity.sharepoint.com/sites/" + sitename))
            {
                SecureString passWord = new SecureString();
                foreach (char c in Pasesword.ToCharArray()) passWord.AppendChar(c);
                context.Credentials = new SharePointOnlineCredentials(usename, passWord);

                GroupCollection siteGroups = context.Web.SiteGroups;

                // Assume that there is a "Members" group, and the ID=5. 
               // Group membersGroup = siteGroups.GetById(8);
                Group membersGroup = context.Web.SiteGroups.GetByName(combogroups.Text);
                context.Load(membersGroup.Users);
                context.ExecuteQuery();

                foreach (User member in membersGroup.Users)
                {
                    // We have all the user info. For example, Title. 
                    //label1.Text = label1.Text + ", " + member.Title;
                    // MessageBox.Show(member.Title);
                    combouser.Items.Add(member.Title);
                }

            }
        }
        public void Getgroups(string usename, string Pasesword, string sitename)
        {
            using (ClientContext context = new ClientContext("https://lahorecity.sharepoint.com/sites/" +sitename))
            {
                SecureString passWord = new SecureString();
                foreach (char c in Pasesword.ToCharArray()) passWord.AppendChar(c);
                context.Credentials = new SharePointOnlineCredentials(usename, passWord);

                Web web = context.Web;
                //Parameters to receive response from the server    
                //SiteGroups property should be passed in Load method to get the collection of groups    
                context.Load(web, w => w.Title, w => w.SiteGroups);
                context.ExecuteQuery();

                GroupCollection groups = web.SiteGroups;

                MessageBox.Show("Groups associated to the site: " + web.Title);
                MessageBox.Show("Groups Count: " + groups.Count.ToString());
                foreach (Group grp in groups)
                {
                    //   MessageBox.Show(grp.Title);
                    combogroups.Items.Add(grp.Title);
                }


            }
        }

        public void Combouser_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string usename = Login.user;
            string pasesword = Login.pass;
            string sitename = comboSC.Text;



            try
            {
                Getgroups(usename, pasesword, sitename);

            }
            catch (Exception ex)
            {
                MessageBox.Show($" There was an exception: {ex.ToString()}");

            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            string usename = Login.user;
            string pasesword = Login.pass;




            try
            {
                adduser(usename, pasesword);

            }
            catch (Exception ex)
            {
                MessageBox.Show($" There was an exception: {ex.ToString()}");

            }
        }
        public  void adduser(string usename, string Pasesword)
        {
            using (ClientContext context = new ClientContext("https://lahorecity.sharepoint.com/sites/" + comboSC.Text))
            {
                SecureString passWord = new SecureString();
                foreach (char c in Pasesword.ToCharArray()) passWord.AppendChar(c);
                context.Credentials = new SharePointOnlineCredentials(usename, passWord);

                /*   // Get group object using group name
                   Group oGroup = context.Web.SiteGroups.GetByName("Demo Members");

                   // Get user using Logon name
                   User oUser = context.Web.EnsureUser("Aryan Khan");

                   context.Load(oUser);
                  context.ExecuteQuery();

                   // Remove user from the group
                   oGroup.Users.RemoveByLoginName(oUser.LoginName);

                   context.ExecuteQuery(); */
                // Get group object using group name
                Group oGroup = context.Web.SiteGroups.GetByName(combogroups.Text);

                // Get user using Logon name
                User oUser = context.Web.EnsureUser(textuseradd.Text);

                // Add user to the group
                oGroup.Users.AddUser(oUser);

                context.ExecuteQuery();
       
            }
        }

        public void deleteuser(string usename, string Pasesword)
        {
            using (ClientContext context = new ClientContext("https://lahorecity.sharepoint.com/sites/" + comboSC.Text))
            {
                SecureString passWord = new SecureString();
                foreach (char c in Pasesword.ToCharArray()) passWord.AppendChar(c);
                context.Credentials = new SharePointOnlineCredentials(usename, passWord);

                   // Get group object using group name
                   Group oGroup = context.Web.SiteGroups.GetByName(combogroups.Text);

                   // Get user using Logon name
                   User oUser = context.Web.EnsureUser(combouser.Text);

                   context.Load(oUser);
                  context.ExecuteQuery();

                   // Remove user from the group
                   oGroup.Users.RemoveByLoginName(oUser.LoginName);

                   context.ExecuteQuery(); 
             
                // Get group object using group name
               /* Group oGroup = context.Web.SiteGroups.GetByName(combogroups.Text);

                // Get user using Logon name
                User oUser = context.Web.EnsureUser(combouser.Text);

                // Add user to the group
                oGroup.Users.AddUser(oUser);

                context.ExecuteQuery(); */

            }
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            string usename = Login.user;
            string pasesword = Login.pass;




            try
            {
                deleteuser(usename, pasesword);

            }
            catch (Exception ex)
            {
                MessageBox.Show($" There was an exception: {ex.ToString()}");

            }
        }

        public void Textuseradd_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            GetItAll obj = new GetItAll();
            obj.Show();
            this.Close();
        }
    }
        }
    
