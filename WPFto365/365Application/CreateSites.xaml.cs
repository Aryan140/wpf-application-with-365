﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.SharePoint.Client;
using System.Security;
using Microsoft.SharePoint;
using OfficeDevPnP.Core.Sites;


namespace _365Application
{
    using Oasys.Wpf.Themes.CustomWindow;

    /// <summary>
    /// Interaction logic for CreateSites.xaml
    /// </summary>
    public partial class CreateSites : OasysWindow
    {
        public CreateSites()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CreateSubsiteTitle obj = new CreateSubsiteTitle();
            obj.Show();
            obj.Close();
            /*
            TeamSiteTitle obj = new TeamSiteTitle();
            obj.Show();
            this.Hide(); */
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            CreateSubsiteTitle obj = new CreateSubsiteTitle();
            obj.Show();
            obj.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            string usename = Login.user;
            string pasesword = Login.pass;




            try
            {
                allusers(usename, pasesword);

            }
            catch (Exception ex)
            {
                MessageBox.Show($" There was an exception: {ex.ToString()}");

            }

        }

        public static void allusers(string usename, string Pasesword)
        {
            using (ClientContext context = new ClientContext("https://lahorecity.sharepoint.com/sites/Attemptclassic"))
            {
                SecureString passWord = new SecureString();
                foreach (char c in Pasesword.ToCharArray()) passWord.AppendChar(c);
                context.Credentials = new SharePointOnlineCredentials(usename, passWord);


                GroupCollection siteGroups = context.Web.SiteGroups;

                // Assume that there is a "Members" group, and the ID=5. 
                Group membersGroup = siteGroups.GetById(7);

                // Let's set up the new user info. 
                UserCreationInformation userCreationInfo = new UserCreationInformation();
                userCreationInfo.Email = "Aryan@Azizltd.onmicrosoft.com";
                userCreationInfo.LoginName = "Aryan khan";
                userCreationInfo.Title = "Aryan khan";

                // Let's add the user to the group. 
                User newUser = membersGroup.Users.Add(userCreationInfo);

                context.ExecuteQuery();


            }
            /*
                            Web web = context.Web;
                            //Parameters to receive response from the server    
                            //SiteGroups property should be passed in Load method to get the collection of groups    
                            context.Load(web, w => w.Title, w => w.SiteGroups);
                            context.ExecuteQuery();

                            GroupCollection groups = web.SiteGroups;

                           MessageBox.Show("Groups associated to the site: " + web.Title);
                           MessageBox.Show("Groups Count: " + groups.Count.ToString());
                            foreach (Group grp in groups)
                            {
                                MessageBox.Show(grp.Title);
                            } */


            //   ---------------------------------------------
            /*
                            GroupCollection siteGroups = context.Web.SiteGroups;

                        // Assume that there is a "Members" group, and the ID=5. 
                        Group membersGroup = siteGroups.GetById(9);
                        context.Load(membersGroup.Users);
                        context.ExecuteQuery();

                        foreach (User member in membersGroup.Users)
                        {
                            // We have all the user info. For example, Title. 
                            //label1.Text = label1.Text + ", " + member.Title;
                            MessageBox.Show(member.Title);
                        } */
        }
    }
}
 

